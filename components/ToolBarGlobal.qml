import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2

import '../js/config.js' as CF
import '../pages'

ToolBar {
    id: toolBarApp

    property alias title: labelName.text
    property alias toolButtonMenu: buttonLeft.toolButtonMenu
    property alias sairRoteiro: buttonLeft.sairRoteiro

    height: 50

    background: Rectangle{
        id: toolBarBackground
        color: CF.backgroundColor
    }

    MessageDialog {
        id: confirmExit
        text: "Sair do Roteiro"
        informativeText: "Confirmar saída?"
        standardButtons: StandardButton.Ok | StandardButton.Cancel
        onAccepted: {
            stackViewPages.pop()
            sairRoteiro = false
        }
    }

    contentItem: Row{

        anchors.fill: parent
        anchors.margins: 5

        ToolButton {
            id: buttonLeft

            property bool toolButtonMenu
            property bool sairRoteiro: false

            icon.source: stackViewPages.currentItem.objectName == "Home" ? ' ' : 'qrc:/media/icons/back.svg'
            icon.color: 'white'

            height: parent.height

            background: Rectangle {
                opacity: (parent.pressed) ? 0.15 : 0
            }

            onClicked: {
                if (stackViewPages.currentItem.objectName == "RoteiroMap"){
                    confirmExit.open();
                }else{
                    stackViewPages.pop();
                    root.currentItem.title = "Home";
                }
            }
        }

        Label {
            id: labelName

            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter

            width: parent.width - (buttonLeft.width + buttonRigth.width)
            height: parent.height

            color: CF.titlePageColor
            font.pointSize: 12
            font.bold: true
            elide: Text.ElideRight
        }

        ToolButton {
            id: buttonRigth
            height: parent.height
            width: height
            visible: false
        }
    }
}
