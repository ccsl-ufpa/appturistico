import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.14
import QtQuick.Layouts 1.14
import QtWebView 1.1

import "../components"

PageGlobal {
    property string site: ""
    Flickable {
        id: flickable
        height: parent.height - tabBar.height - 10
        width: parent.width
        contentHeight: column.height
        maximumFlickVelocity: 5000
        boundsBehavior: Flickable.OvershootBounds
        Column {
            id: column
            width: parent.width
            spacing: 20
            Item {
                height: 100
                width: parent.width
                BackgroundGlobal {
                    id: backgroundPanel
                    anchors.fill: parent

                    Row {
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: 100
                        anchors.margins: 10
                        Image {
                            height: parent.height - 10
                            fillMode: Image.PreserveAspectFit
                            source: 'qrc:/media/icons/logo-ufpa-white.svg'
                            sourceSize.width: 79
                            sourceSize.height: 100
                            smooth: true
                        }
                        Label {
                            color: 'white'
                            text: qsTr('<b>UFPA Turismo</b>')
                            font.pixelSize: parent.height / 3
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            StackLayout {
                id: stackLayout
                currentIndex: tabBar.currentIndex
                height: project.visible ? project.height : software.height
                width: parent.width
                Column {
                    id: project
                    width: parent.width
                    spacing: 20
                    Panel {
                        headerText: qsTr("Projeto")
                        width: parent.width
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr(
                                      "<br><b>UFPA Turismo</b>" + "<br>Aplicativo geoturístico para apoio e incentivo ao turismo de Belém do Pará")
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                            onLinkActivated: utils.openUrl(link,
                                                           Material.primary)
                            linkColor: Material.secondaryTextColor
                        }
                        Column {
                            width: parent.width
                            Label {
                                x: parent.width / 2 - width / 2
                                text: "<br><b>Colaborações e Licença (GPLv3)</b>"
                                horizontalAlignment: Label.AlignHCenter
                            }
                            Row {
                                anchors.horizontalCenter: parent.horizontalCenter
                                ToolButton {
                                    icon {
                                        source: "qrc:/media/icons/gitlab.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    padding: 16
                                    onClicked: {
                                        site = "https://gitlab.com/ccsl-ufpa/appturistico"
                                        popupWeb.open();
                                    }
                                }
                                ToolButton {
                                    icon {
                                        source: "qrc:/media/icons/balance.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    padding: 16
                                    onClicked: {
                                        site = "https://www.gnu.org/licenses/gpl-3.0.txt"
                                        popupWeb.open();
                                    }
                                }
                            }
                        }
                    }
                    Panel {
                        headerText: qsTr("Desenvolvedores")
                        width: parent.width

                        Image {
                            id: ccslLogo
                            source: "qrc:/media/icons/logo-ccsl.svg"
                            fillMode: Image.PreserveAspectFit
                            sourceSize.width: 79
                            sourceSize.height: 120
                            smooth: true
                            x: parent.width / 2 - width / 2
                            horizontalAlignment: Image.AlignHCenter
                        }

                        Column {
                            width: parent.width
                            Label {
                                x: parent.width / 2 - width / 2
                                text: "<br><b>UFPA Turismo</b> é um projeto do <b>Centro de Competência em Software Livre da UFPA</b>"
                                width: parent.width * 0.7
                                horizontalAlignment: Label.AlignHCenter
                                wrapMode: Label.Wrap
                                onLinkActivated: utils.openUrl(link,
                                                               Material.primary)
                                linkColor: Material.secondaryTextColor
                            }
                            Row {
                                anchors.horizontalCenter: parent.horizontalCenter
                                ToolButton {
                                    icon {
                                        source: "qrc:/media/icons/site.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    padding: 16
                                    onClicked: {
                                        site = "http://ccsl.ufpa.br"
                                        popupWeb.open();
                                    }
                                }
                                ToolButton {
                                    icon {
                                        source: "qrc:/media/icons/gitlab.svg"
                                        height: 30
                                        width: 30
                                        color: Material.accent
                                    }
                                    padding: 16
                                    onClicked: {
                                        site = "https://gitlab.com/ccsl-ufpa"
                                        popupWeb.open();
                                    }
                                }
                            }
                        }
                        Repeater {
                            model: ListModel {
                                ListElement {
                                    title: qsTr("Desenvolvedor")
                                    name: qsTr("Vinicius Chaves Botelho")
                                    contacts: [
                                        ListElement {
                                            url: "https://gitlab.com/viniciusdev-br"
                                            sourceIcon: "qrc:/media/icons/gitlab.svg"
                                        },
                                        ListElement {
                                            url: "mailto:viniciusvcb2002@gmail.com"
                                            sourceIcon: "qrc:/media/icons/email.svg"
                                        }
                                    ]
                                }
                                ListElement {
                                    title: qsTr("Orientador")
                                    name: qsTr("Filipe Saraiva")
                                    contacts: [
                                        ListElement {
                                            url: "http://filipesaraiva.info"
                                            sourceIcon: "qrc:/media/icons/site.svg"
                                        },
                                        ListElement {
                                            url: "https://gitlab.com/filipesaraiva"
                                            sourceIcon: "qrc:/media/icons/gitlab.svg"
                                        },
                                        ListElement {
                                            url: "mailto:saraiva@ufpa.br"
                                            sourceIcon: "qrc:/media/icons/email.svg"
                                        }
                                    ]
                                }
                            }
                            delegate: Column {
                                width: parent.width
                                Label {
                                    x: parent.width / 2 - width / 2
                                    text: "<b>" + name + "</b><br>" + title
                                    horizontalAlignment: Label.AlignHCenter
                                }
                                Row {
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    Repeater {
                                        id: repeaterSocial
                                        model: contacts
                                        delegate: ToolButton {
                                            icon {
                                                source: sourceIcon
                                                height: 30
                                                width: 30
                                                color: Material.accent
                                            }
                                            padding: 16
                                            onClicked: {
                                                site = url;
                                                popupWeb.open();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Column {
                    id: software
                    width: parent.width
                    spacing: 20
                    Panel {
                        headerText: qsTr("Base do Software")
                        width: parent.width
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr(
                                      "<br><b>Qt/QML</b>" + "<br>Qt é um framework multiplataforma enquanto " + "QML é uma linguagem de desenvolvimento de interfaces disponível com o Qt")
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                        }
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            ToolButton {
                                icon {
                                    source: "qrc:/media/icons/site.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                padding: 16
                                onClicked: {
                                    site = "https://qt.io";
                                    popupWeb.open();
                                }
                            }
                            ToolButton {
                                icon {
                                    source: "qrc:/media/icons/balance.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                padding: 16
                                onClicked: {
                                    site = "https://www.gnu.org/licenses/gpl-3.0.txt";
                                    popupWeb.open();
                                }
                            }
                        }
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr("<br><b>Material Design</b>" + "<br>Conjunto de componentes de interface independentes de plataforma")
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                        }
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            ToolButton {
                                icon {
                                    source: "qrc:/media/icons/site.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                padding: 16
                                onClicked:{
                                    site = "https://material.io/";
                                    popupWeb.open();
                                }
                            }
                        }
                    }
                    Panel {
                        headerText: qsTr("Mapa")
                        width: parent.width
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width * 0.7
                            text: qsTr(
                                      "<br><b>OpenStreetMap</b>"
                                      + "<br>Projeto comunitário de mapeamento colaborativo "
                                      + "disponibilizando os dados para ampla utilização sob a licença ODbL e os mapas em CC BY SA")
                            horizontalAlignment: Label.AlignHCenter
                            wrapMode: Label.Wrap
                        }
                        Label {
                            x: parent.width / 2 - width / 2
                            text: "<br><b>© contribuidores do OpenStreetMap</b><br>Mapeadores"
                            horizontalAlignment: Label.AlignHCenter
                        }
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            ToolButton {
                                icon {
                                    source: "qrc:/media/icons/site.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                padding: 16
                                onClicked: {
                                    site = "https://www.openstreetmap.org/";
                                    popupWeb.open();
                                }
                            }
                            ToolButton {
                                icon {
                                    source: "qrc:/media/icons/balance.svg"
                                    height: 30
                                    width: 30
                                    color: Material.accent
                                }
                                padding: 16
                                onClicked: {
                                    site = "https://www.openstreetmap.org/copyright";
                                    popupWeb.open();
                                }
                            }
                        }

                    }
                }
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }
    TabBar {
        id: tabBar
        position: TabBar.Footer
        anchors.bottom: parent.bottom
        width: parent.width
        Material.elevation: 6
        Repeater {
            model: ListModel {
                ListElement {
                    title: qsTr("Sobre o Projeto")
                }
                ListElement {
                    title: qsTr("Softwares Integrados")
                }
            }
            delegate: TabButton {
                id: control
                text: title
                font.bold: true
            }
        }
    }
    Popup{
        id: popupWeb
        width: parent.width*0.8; height: parent.height*0.8
        modal: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        padding: 0
        anchors.centerIn: parent
        WebView {
            id: pagweb
            anchors.fill: parent
            url: site
        }
    }
}
