import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Controls.Material 2.14

Column {
    property alias headerText: title.text
    BackgroundGlobal {
        width: parent.width
        height: 30
        Text {
            id: title
            anchors.centerIn: parent
            font.bold: true
            color: Material.primaryHighlightedTextColor
        }
    }
}
