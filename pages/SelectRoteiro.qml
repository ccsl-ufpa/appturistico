import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15
import Qt.labs.platform 1.1

import "../js/config.js" as CF
import '../components'

PageGlobal {
    id: paginaGlobal

    property string targetRoteiro: ""

    ListModel{id:modelDescricao}
    ListModel{id:modelDetalhes}

    Component.onCompleted: {
        var xhr = new XMLHttpRequest;
        xhr.open("GET", StandardPaths.writableLocation(StandardPaths.TempLocation) + "/appturistico/roteiros/listaroteiros.json");
        xhr.onreadystatechange = function() {
            if ( xhr.readyState === XMLHttpRequest.DONE ){
                var data = JSON.parse(xhr.responseText);
                modelDescricao.clear();
                for (var i in data){
                    modelDescricao.append({
                        tag: data[i]['tag']
                    });
                }
            }
        }
        xhr.send()
    }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.centerIn: parent

        Label {
            Layout.alignment: Qt.AlignHCenter

            text: "Selecione o roteiro"
            color: CF.backgroundColor
            font.pointSize: 20
            font.bold: true
        }

        ComboBox {
            id: selectBox

            property bool padraoComboBox: true

            implicitWidth: layout.width * 0.8

            Layout.alignment: Qt.AlignHCenter

            currentIndex: 0
            displayText: padraoComboBox ?  "Selecione: " : currentText

            textRole: "tag"
            model: modelDescricao
            layer.enabled: true
            layer.effect: OpacityMask {
                maskSource: maskSelectBox
            }
            onActivated: {
                var xhr = new XMLHttpRequest;
                xhr.open("GET", StandardPaths.writableLocation(StandardPaths.TempLocation) + "/appturistico/roteiros/listaroteiros.json");
                xhr.onreadystatechange = function() {
                    if ( xhr.readyState === XMLHttpRequest.DONE ){
                        var data = JSON.parse(xhr.responseText);
                        modelDetalhes.clear();
                            modelDetalhes.append({
                                roteiro: data[currentIndex]['roteiro'],
                                bairro: data[currentIndex]['bairro'],
                                detalhes: data[currentIndex]['detalhes'],
                                imagemApoio: data[currentIndex]['imagemApoio']
                            });
                    }
                }
                xhr.send()
                padraoComboBox = false;
            }
        }

        ListView{
            id: listRoteiros
            Layout.preferredWidth: layout.width * 0.8
            Layout.preferredHeight: 300
            Layout.alignment: Qt.AlignHCenter

            model: modelDetalhes
            delegate: Column{
                    id: colunaConteudo

                    Layout.alignment: Qt.AlignHCenter
                    spacing: 10

                    Image {
                        id: imagemRoteiro

                        height: layout.height * 0.45

                        fillMode: Image.PreserveAspectFit
                        source: imagemApoio

                        layer.enabled: true
                        layer.effect: OpacityMask{
                            maskSource: maskImage
                        }
                    }

                    Text {
                        id: textBairro

                        color: CF.backgroundColor
                        font.pointSize: 18;
                        font.bold: true
                        text: bairro
                    }

                    Text {
                        width: layout.width * 0.75

                        wrapMode: Text.WordWrap
                        font.pointSize: 14
                        text: qsTr(detalhes)
                        color: '#565B5B'
                    }
            }
        }

        RoundButton{
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredWidth: layout.width * 0.6
            radius: 5
            Material.background: CF.backgroundColor

            text: "Iniciar roteiro"
            Material.foreground: "white"

            onClicked: {
                root.currentItem.title = "Pontos Turísticos"
                stackViewPages.push("qrc:/pages/RoteiroMap.qml", {"targetRoteiroLast": modelDetalhes.get(listRoteiros.currentIndex).roteiro})
            }
         }
    }
}
