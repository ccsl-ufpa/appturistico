import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12

import '../components'
import "../js/config.js" as CF

PageGlobal {

    id:pageHome
    property string objectName:  "Home"

    ColumnLayout{
        id: layout
        anchors.fill: parent
        anchors.centerIn: parent

        Image {
            id: imagemHome
            Layout.preferredWidth: layout.width * 0.8
            Layout.preferredHeight: layout.height * 0.7

            Layout.maximumHeight: 640
            Layout.maximumWidth: 480

            Layout.alignment: Qt.AlignHCenter

            source: "../media/images/home-page-2.png"

            MouseArea {
                anchors.fill: parent
                onClicked: iniciarRoteiroButton.clicked()
            }
        }

        RoundButton {
            id: iniciarRoteiroButton

            Layout.preferredWidth: imagemHome.width
            Layout.alignment: Qt.AlignHCenter
            radius: 5

            icon.source: "../media/icons/compass-regular.svg"

            text: "Roteiros"
            Material.background: Material.Teal
            Material.foreground: "white"

            onClicked: {
                stackViewPages.push("qrc:/pages/SelectRoteiro.qml")
                root.currentItem.title = "Roteiros";
            }
        }

        Row {
            Layout.alignment: Qt.AlignHCenter
            spacing: 4

            RoundButton{
                id: configurationButton

                width: imagemHome.width / 2
                radius: 5

                icon.source: "../media/icons/configuration.svg"

                text: "Configurações"
                Material.background: Material.Teal
                Material.foreground: "white"

                onClicked:{
                    stackViewPages.push("qrc:/pages/Configuration.qml")
                    root.currentItem.title = "Configurações"
                }
            }

            RoundButton{
                id: aboutButton

                width: imagemHome.width / 2
                radius: 5

                icon.source: "../media/icons/about.svg"

                text: "Sobre"
                Material.background: Material.Teal
                Material.foreground: "white"

                onClicked:{
                    stackViewPages.push("qrc:/pages/About.qml")
                    root.currentItem.title = "Sobre"
                }
            }
        }
    }
}
