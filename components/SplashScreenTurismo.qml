import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.12
import "../js/config.js" as CF

BackgroundGlobal {
    ColumnLayout {
        anchors.centerIn: parent
        Item {
            Layout.preferredWidth: 100
            Layout.fillWidth: true

            Layout.preferredHeight: 125
            Layout.fillHeight: true
            Image {
                id: logo
                Layout.alignment: Qt.AlignHCenter
                source: CF.logoImage
                sourceSize.width: parent.width
            }
            ColorOverlay {
                anchors.fill: logo
                source: logo
                color: "#FFF"
            }
        }
        Label {
            id: label
            Layout.alignment: Qt.AlignHCenter
            color: 'white'
            text: CF.title
        }
    }
}
